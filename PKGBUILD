# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: slact
# Author: Daniel Christophis

pkgname=tlpui-git
pkgver=1.4.0.r212
pkgrel=1
pkgdesc="A GTK-UI to change TLP configuration files easily. It has the aim to protect users from setting bad configuration and to deliver a basic overview of all the valid configuration values."
arch=('any')
url="https://github.com/d4nj1/TLPUI"
license=('GPL-2.0')
groups=()
depends=('tlp' 'python-gobject')
makedepends=('git' 'python-setuptools')
provides=('tlpui')
conflicts=('tlpui')
source=('git+https://github.com/d4nj1/TLPUI.git'
        "https://metainfo.manjariando.com.br/${pkgname}/com.tlpui.metainfo.xml"
        'tlpui.sh')
md5sums=('SKIP'
         '38a343f6613e757b19f144f7888ff767'
         '9f23dcf973bac1089280be1255dfe34d')

pkgver() {
    cd "TLPUI"
    _pkgver=$(git describe --long --tags | sed 's/^tlpui.//;s/\([^-]*-g\)/r\1/;s/-/./g')
    printf '%s.r%s' "$( cut -f1-3 -d'.' <<< ${_pkgver} )" "$(git rev-list --count HEAD)"
}

build() {
    cd "${srcdir}/TLPUI"
    python setup.py build
}

package() {
    cd "${srcdir}/TLPUI"
    python setup.py install --root="$pkgdir/" --optimize=1 --skip-build

    for icon_size in 16 32 48 64 128 96 128 256; do
        icons_dir=usr/share/icons/hicolor/${icon_size}x${icon_size}/apps
        install -d "$pkgdir/$icons_dir"
        install -m644 "${pkgname%-git}/icons/themeable/hicolor/${icon_size}x${icon_size}/apps/${pkgname%-git}.png" -t \
        "$pkgdir/$icons_dir"
    done

    install -Dm644 "${pkgname%-git}/icons/themeable/hicolor/scalable/apps/${pkgname%-git}.svg" -t \
        "$pkgdir/usr/share/icons/hicolor/scalable/apps"

    # Appstream
    install -D -m644 "${srcdir}/com.tlpui.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.tlpui.metainfo.xml"
    mv "${pkgdir}/usr/share/applications/tlpui.desktop" "${pkgdir}/usr/share/applications/com.tlpui.desktop"
}
